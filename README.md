# README #
Keras Layer Description

## Dense()
    
    Dense(
        units, 
        activation=None, 
        use_bias=True, 
        kernel_initializer='glorot_uniform', 
        bias_initializer='zeros', 
        kernel_regularizer=None, 
        bias_regularizer=None, 
        activity_regularizer=None, 
        kernel_constraint=None, 
        bias_constraint=None
    )
    
    Just your regular densely-connected NN layer.
    
    Dense implements the operation: 
    
        output = activation(dot(input, kernel) + bias)
        
    where, activation is the element-wise activation function passed as the activation argument,
    kernel is a weights matrix created by the layer, and bias is a bias vector created by the layer(only applicable if use_bias is True)
    
    Note: if the input to the layer has a rank greater than 2, then it is flattened prior to the initial dot product with kernel.
     
    Example:
    
        # as first layer in a sequential model:
        model = Sequential()
        model.add(Dense(32, input_shape=(16, )))
        # now the model will take as input arrays of shape (*, 16)
        # and output arrays of shape (*, 32)
        
        # after the first layer, you don't need to specify the size of the input anymore:
        model.add(Dense(32))
    
    Arguments:
    
        . units: Positive integer, dimensionality of the output space
        . activation: Activation function to use. If you don't specify anything, no activation is applied(ie. "linear" activation: a(x) = x).
        . use_bias: Boolean, whether the layer uses a bias vector.
        . kernel_initializer: Initializer for the kernel weights matrix (see initializers).
        . bias_initializer: Initializer for the bias vector (see initializers).
        . kernel_regularizer: Regularizer function applied to the kernel weights matrix (see regularizer).
        . bias_regularizer: Regularizer function applied to the bias vector (see regularizer).
        . activity_regularizer: Regularizer function applied to the output of the layer (its "activation"). (see regularizer).            
        . kernel_constraint: Constraint function applied to the kernel weights matrix (see constraints).
        . bias_constraint: Constraint function applied to the bias vector (see constraints).        
        
    Input shape

        nD tensor with shape: (batch_size, ..., input_dim). The most common situation would be a 2D input with shape (batch_size, input_dim).

    Output shape

        nD tensor with shape: (batch_size, ..., units). For instance, for a 2D input with shape  (batch_size, input_dim), the output would have shape (batch_size, units).
        
## Conv2D()

    Conv2D(
        filters,
        kernel_size,
        strides=(1, 1),
        padding='valid',
        data_format=None,
        dilation_rate=(1, 1),
        activation=None,
        use_bias=True,
        kernel_initializer='glorot_uniform',
        bias_initializer='zeros',
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        kernel_constraint=None,
        bias_constraint=None
    )
    
    2D convolution layer(e.g. spatial convolution over images).
    
    This layer creates a convolution kernel that is convolved with the layer input to produce a tensor of outputs.
    If  use_bias is True, a bias vector is created and added to the outputs. Finally, if activation is not None, it is applied to the outputs as well.

    When using this layer as the first layer in a model, provide the keyword argument input_shape (tuple of integers, does not include the sample axis), e.g. input_shape=(128, 128, 3) for 128x128 RGB pictures in  data_format="channels_last".
    
    Arguments:
    
        . filters: Integer, the dimensionality of the output space (i.e. the number of output filters in the convolution).
        . kernel_size: An integer or tuple/list of 2 integers, specifying the height and width of the 2D convolution window. Can be a single integer to specify the same value for all spatial dimensions.
        . strides: An integer or tuple/list of 2 integers, specifying the strides of the convolution along the height and width. Can be a single integer to specify the same value for all spatial dimensions. Specifying any stride value != 1 is incompatible with specifying any dilation_rate value != 1.
        . padding: one of "valid" or "same" (case-insensitive). Note that "same" is slightly inconsistent across backends with strides != 1, as described here
        . data_format: A string, one of "channels_last" or "channels_first". The ordering of the dimensions in the inputs. "channels_last" corresponds to inputs with shape  (batch, height, width, channels) while "channels_first" corresponds to inputs with shape  (batch, channels, height, width). It defaults to the image_data_format value found in your Keras config file at ~/.keras/keras.json. If you never set it, then it will be "channels_last".
        . dilation_rate: an integer or tuple/list of 2 integers, specifying the dilation rate to use for dilated convolution. Can be a single integer to specify the same value for all spatial dimensions. Currently, specifying any dilation_rate value != 1 is incompatible with specifying any stride value != 1. 
        . activation: Activation function to use (see activations). If you don't specify anything, no activation is applied (ie. "linear" activation: a(x) = x).        
        . use_bias: Boolean, whether the layer uses a bias vector.
        . kernel_initializer: Initializer for the kernel weights matrix (see initializers).
        . bias_initializer: Initializer for the bias vector (see initializers).
        . kernel_regularizer: Regularizer function applied to the kernel weights matrix (see regularizer).
        . bias_regularizer: Regularizer function applied to the bias vector (see regularizer).
        . activity_regularizer: Regularizer function applied to the output of the layer (its "activation"). (see regularizer).
        . kernel_constraint: Constraint function applied to the kernel matrix (see constraints).
        . bias_constraint: Constraint function applied to the bias vector (see constraints).

    Input shape

        4D tensor with shape: (samples, channels, rows, cols) if data_format is "channels_first" or 4D tensor with shape: (samples, rows, cols, channels) if data_format is "channels_last".

    Output shape

        4D tensor with shape: (samples, filters, new_rows, new_cols) if data_format is "channels_first" or 4D tensor with shape: (samples, new_rows, new_cols, filters) if data_format is "channels_last". rows and  cols values might have changed due to padding.


## MaxPooling2D()

    MaxPooling2D(
        pool_size=(2, 2),
        strides=None,
        padding='valid',
        data_format=None
    )
    
    Max pooling operation for spatial data.
    
    Arguments:
    
        . pool_size:  integer or tuple of 2 integers, factors by which to downscale (vertical, horizontal). (2, 2) will halve the input in both spatial dimension. 
                      If only one integer is specified, the same window length will be used for both dimensions.
        . strides: Integer, tuple of 2 integers, or None. Strides values. If None, it will default to pool_size.                      
        . padding: One of "valid" or "same" (case-insensitive).
        . data_format: A string, one of channels_last (default) or channels_first. The ordering of the dimensions in the inputs. channels_last corresponds to inputs with shape  (batch, height, width, channels) while channels_first corresponds to inputs with shape  (batch, channels, height, width). It defaults to the image_data_format value found in your Keras config file at ~/.keras/keras.json. If you never set it, then it will be "channels_last".
    
    Input shape

        If data_format='channels_last': 4D tensor with shape:  (batch_size, rows, cols, channels)
        If data_format='channels_first': 4D tensor with shape:  (batch_size, channels, rows, cols)

    Output shape
    
        If data_format='channels_last': 4D tensor with shape:  (batch_size, pooled_rows, pooled_cols, channels)
        If data_format='channels_first': 4D tensor with shape:  (batch_size, channels, pooled_rows, pooled_cols)

    
## Dropout()

    Dropout(
        rate,
        noise_shape=None,
        seed=None
    )
    
    Applies Dropout to the input.
    
    Dropout consists in randomly setting a fraction rate of input units to 0 at each update during training time, which helps prevent overfitting.
    
    Arguments:
    
        . rate: float between 0 and 1. Fraction of the input units to drop.
        . noise_shape: 1D integer tensor representing the shape of the binary dropout mask that will be multiplied with the input. For instance, if your inputs have shape  (batch_size, timesteps, features) and you want the dropout mask to be the same for all timesteps, you can use noise_shape=(batch_size, 1, features).
        . seed: A Python integer to use as random seed.

## Flatten()

    Flatten(
        data_format=None
    )
    
    Flattens the input. Does not affect the batch size.
    
    Arguments:
        
        . data_format: A string, one of channels_last (default) or channels_first. The ordering of the dimensions in the inputs. The purpose of this argument is to preserve weight ordering when switching a model from one data format to another. channels_last corresponds to inputs with shape  (batch, ..., channels) while channels_first corresponds to inputs with shape (batch, channels, ...). It defaults to the image_data_format value found in your Keras config file at  ~/.keras/keras.json. If you never set it, then it will be "channels_last".

    Example
    
        model = Sequential()
        model.add(Conv2D(64, 3, 3, border_mode='same', input_shape=(3, 32, 32)))
        # now: model.output_shape == (None, 64, 32, 32)
        
        model.add(Flatten())
        # now: model.output_shape == (None, 65536)

## Model Compile argument

    loss: String (name of objective function) or objective function.
        If the model has multiple outputs, you can use a different loss
        on each output by passing a dictionary or a list of losses.
        The loss value that will be minimized by the model
        will then be the sum of all individual losses.
        
        function name is as follows.
        
        . 'binary_crossentropy'
        . 'categorical_crossentropy'
        . 'sparse_categorical_crossentropy'
        . 'mean_squared_error'
        . 'mean_absolute_error'
        . 'mean_absolute_percentage_error'
        . 'mean_squared_logarithmic_error'
        . 'squared_hinge'
        . 'hinge'
        . 'categorical_hinge'
        . 'logcosh'
        . 'kullback_leibler_divergence'
        . 'poisson'
        . 'cosine_proximity'
        
    optimizer: String (name of optimizer) or optimizer instance.
    
        . SGD (Stochastic gradient descent) 
        . RMSprop 
        . Adagrad 
        . Adadelta
        . Adam
        . Adamax
        . Nadam
        . TFOptimizer
        
    metrics: List of metrics to be evaluated by the model
            during training and testing.
            Typically you will use `metrics=['accuracy']`.
            To specify different metrics for different outputs of a
            multi-output model, you could also pass a dictionary,
            such as `metrics={'output_a': 'accuracy'}`.
            
## Parameter that decide accuracy of model

    1. Input data (data should be 0 < data < 1)
    2. Model structure
    3. Activator, Loss_function, Optimizer